package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Game;

import java.util.Scanner;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/sample.fxml"));
        Scene scene = new Scene(root,600,400);
        primaryStage.setTitle("Board Games");
        scene.getStylesheets().add("mainscreen.css");
        primaryStage.setScene(scene);

        primaryStage.show();
    }


    public static void main(String[] args) {
        //Run(new MineSweeper(10,10));

        //Player gergo = new Player("geri", 11);
        //Player feri = new Player("feri", 87);
        //Player gabi = new Player("gabi", 31);

        /*DomParser.writeToXml(gergo);
        DomParser.writeToXml(feri);
        DomParser.writeToXml(gabi);*/
        //List<Player> players = DomParser.readFromXml();
        //DomParser.readFromXml();
        //players.stream().forEach(x -> System.out.println(x.getUsername()+ "--" + x.getPoints()));

        launch(args);
    }

    static void Run(Game g)
    {
        Scanner in = new Scanner(System.in);

        int x = 0, y = 0;

        do
        {
            System.out.println(g.toString());
            System.out.println("X = " + x + " Y = " + y);

            x = in.nextInt();
            y = in.nextInt();

            g.shoot(x,y);

        } while (!g.isOver());
        System.out.println();
        System.out.println(g.toString());
        System.out.println("game is over");
    }
}
